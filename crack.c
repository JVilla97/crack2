#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *newHash = md5(guess, strlen(guess));
    // Compare the two hashes
    if(strcmp(newHash, hash) == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
    // Free any malloc'd memory
    free(newHash);
}
// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    int lines = 20;
    char **words = malloc(lines * sizeof(char *));
    
    FILE *f = fopen(filename, "r");
    char line[1000];
    int count = 0;
    
    while(fgets(line, 1000, f) != NULL)
    {
        if(count == lines)
        {
            lines += 20;
            words = realloc(words, lines * sizeof(char *));
        }
        line[strlen(line)-1] = '\0';
        char *word = malloc(strlen(line) * sizeof(char)+1);
        strcpy(word, line);
        words[count] = word;
        count ++;
    }
    *size = count;
    return words;
    fclose(f);
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    FILE *hashes = fopen(argv[1], "r");
    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);
    char lines[HASH_LEN];
    while(fgets(lines, HASH_LEN, hashes) != NULL)
    {
        for(int i = 0; i < dlen; i++)
        {
            char * line2 = dict[i];
            
            if(tryguess(lines, line2) == 1)
            {
                printf("%s %s\n", lines, line2);
            }
            // else
            // {
            //     printf("Incorrect guess\n");
            // }
        }
        //printf("%s\n", lines);
    }
    for(int i = 0; i < dlen; i++)
    {
        free(dict[i]);
    }
    free(dict);
    fclose(hashes);
}
